package ru.example.spring.util;

import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;

@Component
public class TestContext {

    private final Map<String, Object> objectContext = new HashMap<>();

    public <T> T fetch(String alias, Class<T> cls) {
        return (T) objectContext.get(getAlias(cls, alias));
    }

    public <T> void store(String alias, T item) {
        String currentAlias = getAlias(item.getClass(), alias);
        objectContext.put(currentAlias, item);
    }

    private <T> String getAlias(Class<T> cls, String alias) {
        return cls.getSimpleName().toLowerCase() + "-" + alias;
    }
}
