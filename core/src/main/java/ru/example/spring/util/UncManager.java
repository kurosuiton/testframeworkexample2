package ru.example.spring.util;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.example.spring.property.CommonProperty;

import java.util.List;
import java.util.concurrent.TimeUnit;

@Component
public class UncManager {

    private static volatile List<String> availableUnc;

    @Autowired
    private UncManager(CommonProperty commonProperty) {
        availableUnc = commonProperty.getUnc();
    }

    public synchronized String getAvailableUnc() throws Exception {
        for (int tryCount = 0; tryCount < 40; tryCount++) {
            if (availableUnc.size() > 0) {
                String unc = availableUnc.get(0);
                availableUnc.remove(0);
                return unc;
            }
            TimeUnit.SECONDS.sleep(20);
        }
        throw new Exception("Все УНК заняты");
    }

    public synchronized void releaseUnc(String unc) {
        if (!availableUnc.contains(unc)) {
            availableUnc.add(unc);
        }
    }
}
