package ru.example.spring.property;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import java.util.List;

@Configuration
@ConfigurationProperties(prefix = "common")
@Setter
@Getter
public class CommonProperty {
    private String url;
    private String authHeaderWeb;
    private String xFingerPrint;
    private List<String> unc;
    private String password;
    private String otp;
}
