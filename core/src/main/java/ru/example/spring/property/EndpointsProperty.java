package ru.example.spring.property;

import lombok.Getter;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Configuration
@ConfigurationProperties(prefix = "endpoints")
@Getter
@Setter
public class EndpointsProperty {

    @Autowired
    private AuthEndpointsProperty auth;

}
