package ru.example.spring.controller;

import io.qameta.allure.restassured.AllureRestAssured;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.example.spring.property.CommonProperty;
import ru.example.spring.property.EndpointsProperty;

import static io.restassured.RestAssured.given;

@Component
@Slf4j
public class AuthController {

    @Autowired
    private CommonProperty commonProperty;
    @Autowired
    private EndpointsProperty endpointsProperty;

    public Response postOAuth2Token(String login, String password, String otp, String transactionId
            , String sessionDataKey, String grantType, String scope, String authHeader) {
        log.info("Отправляем запрос на авторизацию по УНК и паролю");
        Response response = given().relaxedHTTPSValidation()
                .filter(new AllureRestAssured())
                .baseUri(commonProperty.getUrl())
                .contentType(ContentType.URLENC)
                .header("Authorization", authHeader)
                .header("x-finger-print", commonProperty.getXFingerPrint())
                .formParams("grant_type", grantType,
                        "login", login,
                        "password", password,
                        "scope", scope,
                        "otp", otp,
                        "transactionId", transactionId,
                        "sessionDataKey", sessionDataKey)
                .log().all()
                .post(endpointsProperty.getAuth().getToken());
        log.info(response.asPrettyString());
        return response;
    }
}
