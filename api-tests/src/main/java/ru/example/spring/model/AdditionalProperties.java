package ru.example.spring.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.SneakyThrows;

@Getter
@EqualsAndHashCode(exclude = {"mobile", "sessionDataKey", "transactionId"})
@AllArgsConstructor
@NoArgsConstructor
@Builder
@JsonInclude(JsonInclude.Include.NON_NULL)
public class AdditionalProperties {

    @JsonProperty(value = "domain")
    private String domain;
    @JsonProperty(value = "mobile")
    private String mobile;
    @JsonProperty(value = "sessionDataKey")
    private String sessionDataKey;
    @JsonProperty(value = "transactionId")
    private String transactionId;
    @JsonProperty(value = "username")
    private String username;
    @JsonProperty(value = "lock")
    private String lock;
    @JsonProperty(value = "tech_messages")
    private String techMessages;

    @SneakyThrows
    @Override
    public String toString() {
        return new ObjectMapper().writerWithDefaultPrettyPrinter().writeValueAsString(this);
    }
}
