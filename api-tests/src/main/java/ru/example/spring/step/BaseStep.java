package ru.example.spring.step;

import io.qameta.allure.Step;
import lombok.extern.slf4j.Slf4j;
import org.testng.AssertJUnit;
import ru.example.spring.model.BodyModel;

@Slf4j
public abstract class BaseStep {

    @Step("Проверка кода ответа {expectedCode}")
    public void checkStatusCode(int expectedCode, int actualCode) {
        log.info("Проверяем статус-код");
        AssertJUnit.assertEquals("Не верный статускод", expectedCode, actualCode);
    }

    @Step("Проверка тела ответа")
    protected void equalsResponse(BodyModel expectedBody, BodyModel actualBody) {
        log.info("Проверяем тело ответа");
        log.info("Ожидаемое тело: " + expectedBody);
        log.info("Фактическое тело: " + actualBody);
        AssertJUnit.assertEquals("Ответ не соответствует ожидаемому", expectedBody, actualBody);
    }
}
