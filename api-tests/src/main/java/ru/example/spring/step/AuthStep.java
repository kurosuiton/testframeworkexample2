package ru.example.spring.step;

import io.qameta.allure.Step;
import io.restassured.internal.RestAssuredResponseImpl;
import io.restassured.response.Response;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.testng.AssertJUnit;
import ru.example.spring.controller.AuthController;
import ru.example.spring.enums.Domain;
import ru.example.spring.enums.GrantType;
import ru.example.spring.enums.Message;
import ru.example.spring.enums.Scope;
import ru.example.spring.model.AdditionalProperties;
import ru.example.spring.model.ErrorBodyImpl;
import ru.example.spring.property.CommonProperty;
import ru.example.spring.util.TestContext;

@Component
@Slf4j
public class AuthStep extends BaseStep {
    @Autowired
    private TestContext testContext;
    @Autowired
    private AuthController authController;
    @Autowired
    private CommonProperty commonProperty;

    @Step("Авторизуемся в WEB по УНК и паролю {unc} / {password} в домене мастер")
    public AuthStep loginWebByUnkAndPasswordInMasterFirstFactor(String unc, String password) {
        Response response = authController.postOAuth2Token(unc, password, null, null, null
                , GrantType.LOGIN.getName(), Scope.OPENID.getName(), commonProperty.getAuthHeaderWeb());

        checkStatusCode(401, response.getStatusCode());

        AdditionalProperties additionalProperties = new AdditionalProperties(Domain.MASTER.getName()
                , null
                , null
                , null
                , unc
                , null
                , null);
        ErrorBodyImpl expectedBody = new ErrorBodyImpl(Message.SECOND_FACTOR_REQUIRED.getType()
                , Message.SECOND_FACTOR_REQUIRED.getMessage()
                , Message.SECOND_FACTOR_REQUIRED.getMessageTitle()
                , additionalProperties);
        ErrorBodyImpl actualBody = response.getBody().as(ErrorBodyImpl.class);
        equalsResponse(expectedBody, actualBody);

        String mobile = actualBody.getAdditionalProperties().getMobile();
        String sessionDataKey = actualBody.getAdditionalProperties().getSessionDataKey();
        String transactionId = actualBody.getAdditionalProperties().getTransactionId();
        AssertJUnit.assertTrue("Поле mobile пустое", mobile != null && !mobile.isEmpty());
        AssertJUnit.assertTrue("Поле sessionDataKey пустое", sessionDataKey != null && !sessionDataKey.isEmpty());
        AssertJUnit.assertTrue("Поле transactionId пустое", transactionId != null && !transactionId.isEmpty());

        testContext.store("authResponse", response);
        return this;
    }

    @Step("Авторизуемся в WEB по УНК и не верному паролю {unc} / {password} в домене мастер")
    public AuthStep loginWebByUnkAndUncorrectPasswordInMasterFirstFactor(String unc, String password) {
        Response response = authController.postOAuth2Token(unc, password, null, null, null
                , GrantType.LOGIN.getName(), Scope.OPENID.getName(), commonProperty.getAuthHeaderWeb());

        checkStatusCode(403, response.getStatusCode());

        AdditionalProperties additionalProperties = new AdditionalProperties(null
                , null
                , null
                , null
                , null
                , "TEMPORARY"
                , null);
        ErrorBodyImpl expectedBody = new ErrorBodyImpl(Message.USER_LOCKED.getType()
                , Message.USER_LOCKED.getMessage()
                , Message.USER_LOCKED.getMessageTitle()
                , additionalProperties);
        equalsResponse(expectedBody, response.getBody().as(ErrorBodyImpl.class));

        testContext.store("authResponse", response);
        return this;
    }

    @Step("Авторизуемся в WEB по УНК и паролю {unc} / {password} с ОТП-кодом {otp} в домене мастер")
    public AuthStep loginWebByUnkAndPasswordInMasterSecondFactorOtp(String unc, String password, String otp) {
        Response response = authWithSecondFactor(unc, password, otp);
        checkStatusCode(200, response.getStatusCode());
        return this;
    }

    @Step("Отправка запроса второго фактора авторизации для {unc} / {password} с ОТП-кодом {otp}")
    private Response authWithSecondFactor(String unc, String password, String otp) {
        log.info("Отправляем запрос на авторизацию по УНК и паролю с ОТП-кодом");
        ErrorBodyImpl body = testContext.fetch("authResponse", RestAssuredResponseImpl.class).getBody().as(ErrorBodyImpl.class);
        String transactionId = body.getAdditionalProperties().getTransactionId();
        String sessionDataKey = body.getAdditionalProperties().getSessionDataKey();

        Response response = authController.postOAuth2Token(unc, password, otp, transactionId, sessionDataKey
                , GrantType.LOGIN.getName(), Scope.OPENID.getName(), commonProperty.getAuthHeaderWeb());
        testContext.store("authResponse", response);
        return response;
    }

    public AuthStep loginWebByUnkAndPasswordInMasterFirstFactorWithUncorrectAuthorizationBasic(String unc, String password, String authorization) {
        Response response = authController.postOAuth2Token(unc, password, null, null, null
                , GrantType.LOGIN.getName(), Scope.OPENID.getName(), authorization);

        checkStatusCode(500, response.getStatusCode());

        AdditionalProperties additionalProperties = new AdditionalProperties(null
                , null
                , null
                , null
                , null
                , null
                , "[ExceptionName:AuthenticationException; Message:Authentication failed: Invalid basic authentication info.] ");
        ErrorBodyImpl expectedBody = new ErrorBodyImpl(Message.GENERIC_ERROR.getType()
                , Message.GENERIC_ERROR.getMessage()
                , Message.GENERIC_ERROR.getMessageTitle()
                , additionalProperties);
        equalsResponse(expectedBody, response.getBody().as(ErrorBodyImpl.class));

        testContext.store("authResponse", response);
        return this;
    }
}
