package ru.example.spring.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.ToString;

@AllArgsConstructor
@Getter
@ToString
public enum Message {

    SECOND_FACTOR_REQUIRED("Требуется второй фактор."
            , "second_factor_required"
            , "Произошла ошибка"),
    USER_LOCKED("Сообщение только для К3: инструкция как разблокировать: https://wiki.corp.dev.vtb/x/7btMMg"
                        , "user_locked"
                        , "Вход временно заблокирован"),
    GENERIC_ERROR("Приносим извинения за доставленные неудобства. Попробуйте войти в ВТБ Онлайн позднее."
                        , "generic_error"
                        , "Вход временно невозможен");

    private String message;
    private String type;
    private String messageTitle;
}
