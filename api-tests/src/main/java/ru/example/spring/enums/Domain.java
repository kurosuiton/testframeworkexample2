package ru.example.spring.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.ToString;

@AllArgsConstructor
@Getter
@ToString
public enum Domain {

    MASTER("master");

    private String name;
}
