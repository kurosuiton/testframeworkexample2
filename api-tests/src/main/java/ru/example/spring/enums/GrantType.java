package ru.example.spring.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.ToString;

@AllArgsConstructor
@Getter
@ToString
public enum GrantType {

    LOGIN("login");

    private String name;
}
