package ru.example.spring;

import io.qameta.allure.Epic;
import io.qameta.allure.Feature;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import ru.example.spring.property.CommonProperty;
import ru.example.spring.step.AuthStep;
import ru.example.spring.util.UncManager;

@Epic("Авторизация")
@Feature("УНК и пароль зарегистрированного клиента")
public class AuthTest extends BaseTest {

    @Autowired
    private CommonProperty commonProperty;
    @Autowired
    private AuthStep authStep;
    @Autowired
    private UncManager uncManager;

    private String unc;
    private String password;
    private String otp;

    @BeforeTest(description = "Подготовка тестовых данных")
    public void preCondition() throws Exception {
        unc = uncManager.getAvailableUnc();
        password = commonProperty.getPassword();
        otp = commonProperty.getOtp();
    }

    @Test(description = "Авторизация по УНК и паролю")
    public void loginByUnkAndPassword() {
        authStep.loginWebByUnkAndPasswordInMasterFirstFactor(unc, password);
    }

    @Test(description = "Авторизация по УНК и не верному паролю")
    public void loginByUnkAndUncorrectPassword() {
        authStep.loginWebByUnkAndUncorrectPasswordInMasterFirstFactor(unc, password + "1");
    }

    @Test(description = "Авторизация по УНК и паролю c невалидным Authorization Basic")
    public void loginByUnkAndPasswordWithUncorrectAuthorizationBasic() {
        authStep.loginWebByUnkAndPasswordInMasterFirstFactorWithUncorrectAuthorizationBasic(unc, password, "Basic 123");
    }

    @Test(description = "Авторизация по УНК и паролю со вторым фактором ОТП",
            dependsOnMethods = "loginByUnkAndPassword")
    public void loginByUnkAndPasswordWithSecondFactor() {
        authStep.loginWebByUnkAndPasswordInMasterFirstFactor(unc, password)
                .loginWebByUnkAndPasswordInMasterSecondFactorOtp(unc, password, otp);
    }

    @AfterTest(description = "Освобождаем клиента")
    public void postCondition() {
        uncManager.releaseUnc(unc);
    }

}
